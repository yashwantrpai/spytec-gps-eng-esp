package com.spytecinc.spytecgps;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.crashlytics.android.Crashlytics;
import com.spytecinc.spytecgps.db.DBConnection;

import io.fabric.sdk.android.Fabric;

public class App extends android.support.multidex.MultiDexApplication {
    public static final String PREFS_NAME = "logPrefFile";
    public static DBConnection db;
    public static Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        appContext = this;
        db = new DBConnection(appContext);
    }

    public static Context getContext(){
        return appContext;
    }

    public static DBConnection getDbConnection(){
        return db;
    }

    @Override
    public void onTerminate() {
        db.closeDB();
        super.onTerminate();
    }
}