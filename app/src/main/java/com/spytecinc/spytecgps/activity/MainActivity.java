package com.spytecinc.spytecgps.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.os.AsyncTask;
import java.lang.String;
import java.util.ArrayList;

import android.content.Intent;
import android.widget.TextView;
import android.widget.Toast;

import com.spytecinc.spytecgps.CreatePostString;
import com.spytecinc.spytecgps.CustomHttpClient;
import com.spytecinc.spytecgps.db.DatabaseHelper;
import com.spytecinc.spytecgps.R;
import com.spytecinc.spytecgps.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    Button signin;
    EditText username, password;
    TextView username_title, password_title;
    String response = "true";
    private static final String TAG = "MyActivity";
    DatabaseHelper credentialStore;
    Context context;
    private ProgressDialog pdia;
    public SharedPreferences pref;
    public SharedPreferences.Editor editor;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Create new file with new values
        credentialStore = new DatabaseHelper();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        pref = context.getSharedPreferences("DataStore", MODE_PRIVATE);
        editor = pref.edit();

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            // finally change the color
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorSkyBlue));
        }

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);

        username_title = (TextView) findViewById(R.id.username_title);
        username_title.setText(((pref.getString("language", "0").equals("0"))?getString(R.string.username):getString(R.string.username_esp)));
        password_title = (TextView) findViewById(R.id.password_title);
        password_title.setText(((pref.getString("language", "0").equals("0"))?getString(R.string.password):getString(R.string.password_esp)));

        signin = (Button) findViewById(R.id.signin);
        signin.setText(((pref.getString("language", "0").equals("0"))?getString(R.string.sign_in):getString(R.string.sign_in_esp)));
        signin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if(!username.getText().toString().equalsIgnoreCase("") && !password.getText().toString().equalsIgnoreCase("")) {
                    ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                    if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                        login();
                    } else {
                        Toast.makeText(context, ((pref.getString("language", "0").equals("0"))?getString(R.string.internet_connectivity_check):getString(R.string.internet_connectivity_check_esp )), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if(!isFinishing()) {
                        AlertDialog WrongPasswordAlert = new AlertDialog.Builder(MainActivity.this).create();
                        WrongPasswordAlert.setTitle(((pref.getString("language", "0").equals("0"))?getString(R.string.login_failed):getString(R.string.login_failed_esp)));
                        WrongPasswordAlert.setMessage(((pref.getString("language", "0").equals("0"))?getString(R.string.enter_credentials_to_login):getString(R.string.enter_credentials_to_login_esp)));
                        WrongPasswordAlert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        WrongPasswordAlert.show();
                    }
                }
            }
        });
    }

    public void login() {
        String urls[] = getResources().getStringArray(R.array.url);
        String currentServer = pref.getString("currentServer", urls[0]);
        if(Util.isValidUrl(currentServer, urls)) {
            String loginUrl = currentServer + ((pref.getString("language", "0").equals("0"))?getString(R.string.login_url):getString(R.string.login_url_esp));
            ArrayList params = new ArrayList();
            Pair pair = new Pair("operation", "login");
            params.add(pair);
            pair = new Pair("username", username.getText().toString());
            params.add(pair);
            pair = new Pair("password", password.getText().toString()); // android.telephony.TelephonyManager.getDeviceId()
            params.add(pair);

            String postString = CreatePostString.createPostString(params);
            Log.i("param", postString);
            loginTask loginTask = new loginTask();
            loginTask.execute(loginUrl, postString);
        } else {
            Toast.makeText(context, ((pref.getString("language", "0").equals("0"))?getString(R.string.server_connection_issue):getString(R.string.server_connection_issue_esp)), Toast.LENGTH_SHORT).show();
            Intent i = new Intent(context, SplashScreen.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if ( pdia!=null && pdia.isShowing() ){
            pdia.cancel();
        }
    }

    private class loginTask extends AsyncTask<String, Void, String> implements DialogInterface.OnCancelListener{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(!isFinishing() && android.os.Build.VERSION.SDK_INT > 19) {
                pdia = new ProgressDialog(context);
                pdia.setMessage(((pref.getString("language", "0").equals("0"))?getString(R.string.validating):getString(R.string.validating_esp)));
                pdia.setCancelable(false);
                pdia.show();
            }
        }

        @Override
        protected String doInBackground(String... url) {
            // TODO Auto-generated method stub
            String res = null;
            try {
                response = CustomHttpClient.executeHttpPost(url[0], url[1]);
                res = response.toString();
                // Log.d("Response",res);
                //res= res.replaceAll("\\s+","");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            if(!isFinishing()) {
                if (pdia != null && pdia.isShowing()) {
                    pdia.dismiss();
                }
            }
            response = result;

            if (response != null && !response.equals("")) {
                // Log.d(TAG, "Enable_login response: "+response);
                JSONObject respjson = new JSONObject();
                if (!response.equals("")) {
                    try {
                        respjson = new JSONObject(response);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        //Log.d(TAG, response);
                        if (response != null && response.contains("true")) {
                            JSONObject data = new JSONObject();
                            try {
                                data = respjson.getJSONObject("data");
                                String authcode = data.getString("authcode");
                                boolean isInserted = credentialStore.setFlag(1, username.getText().toString(), password.getText().toString(), authcode);
                                Intent i = new Intent(MainActivity.this, HomeActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            if(!isFinishing()) {
                                AlertDialog WrongPasswordAlert = new AlertDialog.Builder(MainActivity.this).create();
                                WrongPasswordAlert.setTitle(((pref.getString("language", "0").equals("0"))?getString(R.string.login_failed):getString(R.string.login_failed_esp)));
                                WrongPasswordAlert.setMessage((pref.getString("language", "0").equals("0"))?getString(R.string.authentication_error):getString(R.string.authentication_error_esp));
                                WrongPasswordAlert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });
                                WrongPasswordAlert.show();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }//close onPostExecute
        }// close loginTask

        @Override
        public void onCancel(DialogInterface dialog) {
            cancel(true);
        }
    }
}
