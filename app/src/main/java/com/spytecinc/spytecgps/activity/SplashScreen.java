package com.spytecinc.spytecgps.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;
import com.spytecinc.spytecgps.CustomHttpClient;
import com.spytecinc.spytecgps.db.DatabaseHelper;
import com.spytecinc.spytecgps.R;
import com.spytecinc.spytecgps.WebkitCookieManagerProxy;
import com.squareup.okhttp.OkHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.net.CookieHandler;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.URL;
import java.net.UnknownHostException;

public class SplashScreen extends AppCompatActivity {
    DatabaseHelper credentialStore;

    public static OkHttpClient OkHTTPClient;
    public static WebkitCookieManagerProxy coreCookieManager;
    public static String response;
    public static String currentServer="";
    public static Context context;
    public static String TAG = "SplashScreen";
    public SharedPreferences pref;
    public SharedPreferences.Editor editor;
    public TextView please_wait;

    public static String getCurrentServer() {
        return currentServer;
    }

    public boolean checkServer(String url) {
        Boolean exists = false;
        InetAddress address;
        try {
            address = InetAddress.getByName(new URL(url).getHost());
            SocketAddress sockaddr = new InetSocketAddress(address, 80);
            // Create an unbound socket
            Socket sock = new Socket();

            // This method will block no more than timeoutMs.
            // If the timeout occurs, SocketTimeoutException is thrown.
            int timeoutMs = 5000;   // 5 seconds
            sock.connect(sockaddr, timeoutMs);
            exists = true;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            Context context = SplashScreen.this.getApplicationContext();
            Toast.makeText(context, (pref.getString("language", "0").equals("0"))?R.string.internet_connectivity_check:R.string.internet_connectivity_check_esp, Toast.LENGTH_SHORT).show();
        }
        return exists;
    }
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        context = this;
        pref = context.getSharedPreferences("DataStore", MODE_PRIVATE);
        editor = pref.edit();

        please_wait = (TextView) findViewById(R.id.please_wait);
        please_wait.setText((pref.getString("language", "0").equals("0"))?R.string.please_wait:R.string.please_wait_esp);
        credentialStore = new DatabaseHelper();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            Window window = this.getWindow();

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            // finally change the color
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorSkyBlue));
        }

        //bar = (ProgressBar) this.findViewById(R.id.progressBar);

        // This method will be executed once the timer is over
        // Start your app main activity
        new checkServerTask().execute();
        // close this activity
    }

    public static OkHttpClient getClient() {
        if(OkHTTPClient == null) {
            //cookieJar = new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
            //OkHTTPClient = new OkHttpClient.Builder()
                    //.cookieJar(cookieJar)
                    //.build();
            OkHTTPClient = new OkHttpClient();
            // initialize WebkitCookieManagerProxy, set Cookie Policy, create android.webkit.webkitCookieManager
            coreCookieManager = new WebkitCookieManagerProxy(null, java.net.CookiePolicy.ACCEPT_ALL);
            CookieHandler.setDefault(coreCookieManager);
            OkHTTPClient.setCookieHandler(coreCookieManager);
        }
        return OkHTTPClient;
    }

    public void getSessionCookie(Context activity_context){
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        String url = pref.getString("currentServer", "") + ((pref.getString("language", "0").equals("0"))?getString(R.string.login_url):getString(R.string.login_url_esp));
        JSONObject params = new JSONObject();
        try {
            params.put("username", credentialStore.getClient().toString());
            params.put("password", credentialStore.getPasscode().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            if(activity_context == SplashScreen.this){
                SplashScreen.getSessionLoadMaps t1 = new SplashScreen.getSessionLoadMaps();
                t1.execute(url, params.toString());
            }else {
                SplashScreen.validateUserTask t2 = new SplashScreen.validateUserTask();
                t2.execute(url, params.toString());
            }
        }else{
            Context context = activity_context.getApplicationContext();
            Toast.makeText(context, (pref.getString("language", "0").equals("0"))?R.string.internet_connectivity_check:R.string.internet_connectivity_check_esp, Toast.LENGTH_SHORT).show();
            Intent i = new Intent(activity_context, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }
    }

    public class validateUserTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String url[]) {
            // TODO Auto-generated method stub
            String res = null;
            try {
                response = CustomHttpClient.executeHttpPost(url[0], url[1]);
                res = response.toString();
                //res= res.replaceAll("\\s+","");
            } catch (Exception e) {
                //txt_Error.setText(e.toString());
            }
            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            response = result;
            Log.d(TAG, "Enable_login response: "+response);
        }
    }

    public class checkServerTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute(){
            //bar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String url[]) {
            // TODO Auto-generated method stub
            String res = null;
            try {
                String urls[] = getResources().getStringArray(R.array.url);
                while(true){
                    if(checkServer(urls[0])){
                        currentServer = urls[0];
                        editor.putString("currentServer", urls[0]);
                        editor.commit();
                        break;
                    }
                    else if(checkServer(urls[1])){
                        currentServer = urls[1];
                        editor.putString("currentServer", urls[1]);
                        editor.commit();
                        break;
                    }
                }
                //res= res.replaceAll("\\s+","");
            } catch (Exception e) {
                //txt_Error.setText(e.toString());
            }
            //return res;
            return "";
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            //bar.setVisibility(View.GONE);

            Log.d(TAG, "CurrentServer "+currentServer);
            if (credentialStore.checkFlag()) {
                getSessionCookie(SplashScreen.this);
            }
            else {
                Intent i = new Intent(SplashScreen.this, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        }
    }

    public class getSessionLoadMaps extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String url[]) {
            // TODO Auto-generated method stub
            String res = null;
            try {
                response = CustomHttpClient.executeHttpPost(url[0], url[1]);
                res = response.toString();
                //res= res.replaceAll("\\s+","");
            } catch (Exception e) {
                //txt_Error.setText(e.toString());
            }
            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            response = result;
            Intent i = new Intent(SplashScreen.this, HomeActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }
    }
}
