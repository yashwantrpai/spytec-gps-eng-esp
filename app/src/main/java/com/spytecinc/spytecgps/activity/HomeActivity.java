package com.spytecinc.spytecgps.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.spytecinc.spytecgps.App;
import com.spytecinc.spytecgps.db.DatabaseHelper;
import com.spytecinc.spytecgps.R;
import com.spytecinc.spytecgps.fragment.dtc;
import com.spytecinc.spytecgps.fragment.dtcform;
import com.spytecinc.spytecgps.fragment.fuelreport;
import com.spytecinc.spytecgps.fragment.fuelreportform;
import com.spytecinc.spytecgps.fragment.help;
import com.spytecinc.spytecgps.fragment.home;
import com.spytecinc.spytecgps.fragment.mileage;
import com.spytecinc.spytecgps.fragment.mileageform;
import com.spytecinc.spytecgps.fragment.reports;
import com.spytecinc.spytecgps.fragment.trackhistory;
import com.spytecinc.spytecgps.fragment.trackhistoryform;
import com.spytecinc.spytecgps.fragment.travellog;
import com.spytecinc.spytecgps.fragment.travellogform;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;
    private ImageView imgNavHeaderBg, imgProfile;
    private TextView txtName, txtWebsite;
    private Toolbar toolbar;
    public Boolean isFragmentRendered;
    //private FloatingActionButton fab;

    // urls to load navigation header background image
    // and profile image
    //private static final String urlNavHeaderBg = "http://api.androidhive.info/images/nav-menu-header-bg.jpg";
    //private static final String urlProfileImg = "https://lh3.googleusercontent.com/eCtE_G34M9ygdkmOpYvCag1vBARCmZwnVS6rS5t4JLzJ6QgQSBquM0nuTsCpLhYbKljoyS-txg";

    // tags used to attach the fragments
    public static final String TAG_HOME = "location";
    public static final String TAG_REPORTS = "reports";
    public static final String TAG_DTC = "dtc";
    public static final String TAG_DTC_FORM = "dtcform";
    public static final String TAG_FUELREPORT = "fuelreport";
    public static final String TAG_FUELREPORT_FORM = "fuelreportform";
    public static final String TAG_MILEAGE = "mileage";
    public static final String TAG_MILEAGE_FORM = "mileageform";
    public static final String TAG_TRACKHISTORY = "trackhistory";
    public static final String TAG_TRACKHISTORY_FORM = "trackhistoryform";
    public static final String TAG_TRAVELLOG = "travellog";
    public static final String TAG_TRAVELLOG_FORM = "travellogform";
    public static final String TAG_CHANGE_PASSWORD = "changepassword";
    public static final String TAG_HELP = "help";
    public static final String TAG_LOGOUT = "logout";
    public static Context context;
    DatabaseHelper myDB;
    public static ArrayList values = new ArrayList();
    // toolbar titles respected to selected nav menu item
    private  static String[] activityTitles;
    public static LinearLayout progressLayout;
    public static ProgressBar progressBar;
    public static TextView progressText;

    // flag to load home fragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;
    public static SharedPreferences pref;
    public static SharedPreferences.Editor editor;
    public Bundle currentInstanceState;
    public static Activity activity;
    public HomeActivity() {
        isFragmentRendered = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentInstanceState = savedInstanceState;
        activity = this;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP){
            Window window = activity.getWindow();

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            // finally change the color
            window.setStatusBarColor(ContextCompat.getColor(activity, R.color.colorSkyBlue));
        }

        RenderCurrentFragment();
    }

    public void RenderCurrentFragment(){
        context = this;
        myDB = new DatabaseHelper();
        pref = context.getSharedPreferences("DataStore", MODE_PRIVATE);
        editor = pref.edit();
        if(pref.getString("curfile", "") == null) {
            editor.putString("curfile", "home");
        }

        if (myDB.checkFlag() == false) {
            Intent i = new Intent(this, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        } else {
            setContentView(R.layout.activity_home);

            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            mHandler = new Handler();

            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setItemIconTintList(null);

            // Navigation view header
            navHeader = navigationView.getHeaderView(0);
            // txtName = (TextView) navHeader.findViewById(R.id.name);
            txtWebsite = (TextView) navHeader.findViewById(R.id.website);
            imgNavHeaderBg = (ImageView) navHeader.findViewById(R.id.img_header_bg);
            imgProfile = (ImageView) navHeader.findViewById(R.id.img_profile);

            progressLayout = (LinearLayout) findViewById(R.id.progressLayout);
            progressBar = (ProgressBar) findViewById(R.id.progressbar);
            progressText = (TextView) findViewById(R.id.progressText);

            // load toolbar titles from string resources
            activityTitles = getResources().getStringArray((pref.getString("language", "0").equals("0"))?R.array.nav_item_activity_titles:R.array.nav_item_activity_titles_esp);

            // load nav menu header data
            loadNavHeader();

            // initializing navigation menu
            setUpNavigationView();

            int orientation = getResources().getConfiguration().orientation;

            //if (currentInstanceState == null) {
            //  editor.putString("CURRENT_TAG", TAG_HOME);
            //  loadHomeFragment();
            // } else if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            loadHomeFragment();
            // }

            isFragmentRendered = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(!isFragmentRendered) {
            // if(pref.getString("keepSession", "0").equals("1")){
                RenderCurrentFragment();
            /* } else {
                Intent i = new Intent(App.getContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(i);
            } */
        } else {
            isFragmentRendered = false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    /***
     * Load navigation menu header information
     * like background image, profile image
     * name, website, notifications action view (dot)
     */
    private void loadNavHeader() {
        // name, website
        // txtName.setText("GPS & Fleet");
        txtWebsite.setText("www.spytecinc.com");

        // loading header background image
    }

    /***
     * Returns respected fragment that user
     * selected from navigation menu
     */
    private void loadHomeFragment() {
        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        // if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
        // drawer.closeDrawers();

        // show or hide the fab button
        //toggleFab();
        // return;
        // }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        //Runnable mPendingRunnable = new Runnable() {
        //@Override
        //public void run() {
        // update the main content by replacing fragments
        Fragment fragment = getHomeFragment();
        String CURRENT_TAG = pref.getString("CURRENT_TAG", TAG_HOME);
        if(fragment!=null)
        {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
            fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
            fragmentTransaction.commit();
        }
        //}
        //};
        // set toolbar title
        setToolbarTitle();

        // selecting appropriate nav menu item
        selectNavMenu();
        // If mPendingRunnable is not null, then add to the message queue
        //if (mPendingRunnable != null) {
        //mHandler.post(mPendingRunnable);
        //}

        // show or hide the fab button
        //toggleFab();

        //Closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    private Fragment getHomeFragment() {
        String CUR_TAG = pref.getString("CURRENT_TAG", TAG_HOME);
        switch (CUR_TAG) {
            case TAG_HELP:
                help helpFragment = new help();
                return helpFragment;
            case TAG_HOME:
                editor.putString("CURRENT_TAG", TAG_HOME);
                editor.commit();
                home homeFragment = new home();
                return homeFragment;
            case TAG_REPORTS:
                reports reportsFragment = new reports();
                return reportsFragment;
            case TAG_DTC:
                dtc dtcFragment = new dtc();
                return dtcFragment;
            case TAG_DTC_FORM:
                dtcform dtcformFragment = new dtcform();
                return dtcformFragment;
            case TAG_FUELREPORT:
                fuelreport fuelreportFragment = new fuelreport();
                return fuelreportFragment;
            case TAG_FUELREPORT_FORM:
                fuelreportform fuelreportformFragment = new fuelreportform();
                return fuelreportformFragment;
            case TAG_MILEAGE:
                mileage mileageFragment = new mileage();
                return mileageFragment;
            case TAG_MILEAGE_FORM:
                mileageform mileageformFragment = new mileageform();
                return mileageformFragment;
            case TAG_TRAVELLOG:
                travellog travellogFragment = new travellog();
                return travellogFragment;
            case TAG_TRAVELLOG_FORM:
                travellogform travellogformFragment = new travellogform();
                return travellogformFragment;
            case TAG_TRACKHISTORY:
                trackhistory trackhistoryFragment = new trackhistory();
                return trackhistoryFragment;
            case TAG_TRACKHISTORY_FORM:
                trackhistoryform trackhistoryformFragment = new trackhistoryform();
                return trackhistoryformFragment;
            case TAG_LOGOUT:
                // logout fragment
                logoutAlert();
                break;
            default:
                return new home();
        }
        return new home();
    }

    private void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[getTitleIndex(pref.getString("CURRENT_TAG", TAG_HOME))]);
        Log.d("fragment",activityTitles[getTitleIndex(pref.getString("CURRENT_TAG", TAG_HOME))]);
    }

    private void selectNavMenu() {
        if(navigationView.getCheckedItem() != null) {
            navigationView.getCheckedItem().setChecked(false);
        }
        int itemIndex = getTitleIndex(pref.getString("CURRENT_TAG", TAG_HOME));
        navigationView.getMenu().getItem(itemIndex).setChecked(true);
    }

    public int getTitleIndex(String tag) {
        int tag_index = 0;

        if (tag.equals(TAG_HOME)) {
            tag_index = 0;
        } else if ((tag.equals(TAG_REPORTS)) || (tag.equals(TAG_DTC)) || (tag.equals(TAG_DTC_FORM)) || (tag.equals(TAG_FUELREPORT)) || (tag.equals(TAG_FUELREPORT_FORM)) || (tag.equals(TAG_MILEAGE)) || (tag.equals(TAG_MILEAGE_FORM)) || (tag.equals(TAG_TRAVELLOG)) || (tag.equals(TAG_TRAVELLOG_FORM))){
            tag_index = 1;
        } else if(tag.equals(TAG_TRACKHISTORY) || tag.equals(TAG_TRACKHISTORY_FORM)) {
            tag_index = 2;
        } else if(tag.equals(TAG_HELP)) {
            tag_index = 3;
        }
        return tag_index;
    }

    private void setUpNavigationView() {
        navigationView.getMenu().getItem(0).setTitle((pref.getString("language", "0").equals("0"))?R.string.nav_location:R.string.nav_location_esp);
        navigationView.getMenu().getItem(1).setTitle((pref.getString("language", "0").equals("0"))?R.string.nav_reports:R.string.nav_reports_esp);
        navigationView.getMenu().getItem(2).setTitle((pref.getString("language", "0").equals("0"))?R.string.nav_trackhistory:R.string.nav_trackhistory_esp);
        navigationView.getMenu().getItem(3).setTitle((pref.getString("language", "0").equals("0"))?R.string.nav_help:R.string.nav_help_esp);
        navigationView.getMenu().getItem(4).setTitle((pref.getString("language", "0").equals("0"))?R.string.nav_logout:R.string.nav_logout_esp);

        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                boolean flag = true, loadFrag = true;
                String CURRENT_TAG = pref.getString("CURRENT_TAG", TAG_HOME);
                if(CURRENT_TAG.equalsIgnoreCase(TAG_TRACKHISTORY)){
                    if(trackhistory.isInfoWindowOpen()){
                        Toast.makeText(context, (pref.getString("language", "0").equals("0"))?R.string.close_infowindow_before_navigating:R.string.close_infowindow_before_navigating_esp, Toast.LENGTH_LONG).show();
                        flag = false;
                        editor.putString("CURRENT_TAG", TAG_TRACKHISTORY);
                        editor.commit();
                        setToolbarTitle();
                        selectNavMenu();
                        drawer.closeDrawers();
                        invalidateOptionsMenu();
                    }
                }

                if(flag) {
                    CURRENT_TAG = TAG_HOME;
                    //Check to see which item was being clicked and perform appropriate action
                    switch (menuItem.getItemId()) {
                        //Replacing the main content with ContentFragment Which is our Inbox View;
                        case R.id.nav_location:
                            editor.putString("CURRENT_TAG", TAG_HOME);
                            menuItem.setChecked(true);
                            break;
                        case R.id.nav_reports:
                                editor.putString("CURRENT_TAG", TAG_REPORTS);
                                menuItem.setChecked(true);
                            break;
                        case R.id.nav_trackhistory:
                                editor.putString("CURRENT_TAG", TAG_TRACKHISTORY_FORM);
                                menuItem.setChecked(true);
                            break;
                        case R.id.nav_help:
                            editor.putString("CURRENT_TAG", TAG_HELP);
                            menuItem.setChecked(true);
                            break;
                        case R.id.nav_logout:
                            logoutAlert();
                            break;
                        default:
                            CURRENT_TAG = TAG_HOME;
                            editor.putString("CURRENT_TAG", CURRENT_TAG);
                    }
                    editor.commit();

                    if(loadFrag) {
                        loadHomeFragment();
                    }
                }

                return true;
            }
        });

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {
            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                if(navigationView.getCheckedItem() != null) {
                    if ((navigationView.getCheckedItem().getItemId() == R.id.nav_reports)) {
                        navigationView.getCheckedItem().setChecked(false);
                    }
                    if ((navigationView.getCheckedItem().getItemId() == R.id.nav_trackhistory)) {
                        navigationView.getCheckedItem().setChecked(false);
                    }
                    int itemIndex = getTitleIndex(pref.getString("CURRENT_TAG", TAG_HOME));
                    navigationView.getMenu().getItem(itemIndex).setChecked(true);
                }
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if(menu != null) {
            for (int i = 0; i < menu.size(); i++) {
                menu.getItem(i).setVisible(false);
            }
        }
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        // This code loads home fragment when back key is pressed
        // when user is in other fragment than home
        if (shouldLoadHomeFragOnBackPress) {
            // checking if user is on other navigation menu
            // rather than home
            if (pref.getString("CURRENT_TAG", TAG_HOME) != TAG_HOME) {
                editor.putString("CURRENT_TAG", TAG_HOME);
                loadHomeFragment();
                return;
            }
        }

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        // show menu only when home fragment is selected
        getMenuInflater().inflate(R.menu.main, menu);
        //Calling a method to set the backgournd color of the options menu
        //setMenuBackground();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            logoutAlert();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static boolean equalStrings(String sb1, String sb2) {
        int len = sb1.length();
        if (len != sb2.length()) {
            return false;
        }
        for (int i = 0; i < len; i++) {
            if (sb1.charAt(i) != sb2.charAt(i)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Sets the menu items background.
     */
    private void setMenuBackground() {
        /** Step 1. setting the custom LayoutInflater.Factory instance. */
        getLayoutInflater().setFactory(new LayoutInflater.Factory() {

            /**
             * Step 2. Implementing the onCreateView method
             * that will actually set the background selector.
             * {@inheritDoc}
             */
            public View onCreateView(final String name, final Context context, final AttributeSet attributeSet) {
                /**
                 *  Step 3. Checking if the view that is to be created
                 *  is IconMenuItemView.
                 *  Notice that this is an internal class.
                 */
                if (name.equalsIgnoreCase
                        ("com.android.internal.view.menu.IconMenuItemView")) {
                    try {
                        /**
                         * Step 4. If the view is IconMenuItemView then
                         * create the view using the LayoutInflater.
                         */
                        final LayoutInflater f = getLayoutInflater();
                        final View view = f.createView(name, null, attributeSet);
                        /**
                         * Step 5. This is the key part.
                         * The view instance that was created in step 4
                         * is an instance of IconMenuItemView.
                         * This is the view whose background color
                         * we want to change.  Unfortunately we just cannot
                         * change the background color at this place,
                         * since even if we change it here,
                         * framework overrides this value and
                         * we see the default background selector.
                         * Because of this reason the below line is commented.
                         * It does not work.
                         */
                        //view.setBackgroundResource(R.drawable.menu_selector);
                        /**
                         * Step 6. We have to change the background color
                         * after the view has rendered, using the Handler api.
                         */
                        new Handler().post(new Runnable() {
                            public void run() {
                                /** Step 7.  Changing the backgound color. */
                                view.setBackgroundResource(
                                        R.drawable.menu_selector);
                            }
                        });
                        return view;
                    } catch (final Exception e) {
                        /**
                         * Step 8.  Catching all exceptions that could occur
                         * in the process.  This is necessary since
                         * on Android 2.3, styling the internal
                         * IconMenuItemView throws an exception.
                         * Hence we have to fallback to the default menu styles.
                         */
                        Log.e("##Menu##", "Could not create a custom view for menu: " + e.getMessage(), e);
                    }
                }
                return null;
            }
        });
    }

    public static void logoutAlert(){
        AlertDialog SignOutAlert = new AlertDialog.Builder(activity).create();
        SignOutAlert.setTitle((pref.getString("language", "0").equals("0"))?R.string.logout_alert:R.string.logout_alert_esp);
        SignOutAlert.setButton(AlertDialog.BUTTON_POSITIVE, activity.getResources().getString((pref.getString("language", "0").equals("0"))?R.string.yes:R.string.yes_esp),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        DatabaseHelper myDB = new DatabaseHelper();
                        boolean isInserted = myDB.setFlag(0, "", "", "");
                        SharedPreferences pref = context.getSharedPreferences("DataStore", MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();

                        editor.putString("drivers", "");
                        editor.putString("CURRENT_TAG", TAG_HOME);
                        editor.commit();
                        Intent i = new Intent(App.getContext(), MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(i);
                    }
                });
        SignOutAlert.setButton(AlertDialog.BUTTON_NEGATIVE, activity.getResources().getString((pref.getString("language", "0").equals("0"))?R.string.no:R.string.no_esp),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        SignOutAlert.show();
    }

    public static void logout(){
        DatabaseHelper myDB = new DatabaseHelper();
        boolean isInserted = myDB.setFlag(0, "", "", "");
        SharedPreferences pref = context.getSharedPreferences("DataStore", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        /* if(pref.getString("keepSession", "0").equalsIgnoreCase("0")) {
            editor.putString("drivers", "");
            editor.putString("CURRENT_TAG", TAG_HOME);
            editor.commit();
        } */
    }
}