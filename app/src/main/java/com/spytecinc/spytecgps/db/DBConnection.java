package com.spytecinc.spytecgps.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.spytecinc.spytecgps.db.DatabaseHelper;

public class DBConnection extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 4;

    public static String DBNAME = "hosdbsqlite.db.sql";

    private SQLiteDatabase myDataBase;
    public static Context context;
    public DBConnection(Context context) {
        super(context, DBNAME, null, DATABASE_VERSION);
        this.context = context;
    }

    // Creating database
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i("dbcreate", "db_version: " + db.getVersion());
        DatabaseHelper.create(db);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    // closing database
    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    @Override
    public synchronized void close() {

        if(myDataBase != null)
            myDataBase.close();
        super.close();
    }

    public SQLiteDatabase getWritableDatabaseInstance() {
        return this.getWritableDatabase();
    }

    public SQLiteDatabase getReadableDatabaseInstance() {
        return this.getReadableDatabase();
    }
}

