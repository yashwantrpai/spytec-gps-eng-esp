package com.spytecinc.spytecgps.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.spytecinc.spytecgps.App;

public class DatabaseHelper{
    public static final String DATABASE_NAME = "SpytecDB.db";
    public static final String TABLE_NAME = "SPYTECGPS_FLAG_TABLE";
    public static final String ID = "ID";
    public static final String EMAIL = "EMAIL";
    public static final String PASSCODE = "PASSCODE";
    public static final String FLAG = "FLAG";
    public static final String AUTHCODE = "AUTHCODE";
    public static SQLiteDatabase db;

    public void DatabaseHelper(){

    }

    public static void create(SQLiteDatabase db) {
        Cursor res = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table' AND name='"+TABLE_NAME+"'", null);
        if(res.getCount() == 0){
            db.execSQL("CREATE TABLE " + TABLE_NAME + "(ID INTEGER, FLAG INTEGER, EMAIL VARCHAR, PASSCODE VARCHAR, AUTHCODE VARCHAR)");
            ContentValues contentValues = new ContentValues();
            contentValues.put(ID, 0);
            contentValues.put(FLAG, 0);
            contentValues.put(EMAIL, "");
            contentValues.put(PASSCODE, "");
            contentValues.put(AUTHCODE, "");
            long result = db.insert(TABLE_NAME, null, contentValues);
        }
    }

    public boolean setFlag(Integer flag, String email, String passcode, String authcode){
        SQLiteDatabase db = App.getDbConnection().getWritableDatabaseInstance();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ID, 0);
        contentValues.put(FLAG, flag);
        contentValues.put(EMAIL, email);
        contentValues.put(PASSCODE, passcode);
        contentValues.put(AUTHCODE, authcode);
        long result = db.update(TABLE_NAME, contentValues, "id = 0", null);
        db.close();
        if(result == -1){
            return false;
        }else{
            return true;
        }
    }

    public boolean setLogoutFlag(Integer flag){
        SQLiteDatabase db = App.getDbConnection().getWritableDatabaseInstance();
        ContentValues contentValues = new ContentValues();
        contentValues.put(FLAG, flag);
        long result = db.update(TABLE_NAME, contentValues, "id = 0", null);
        db.close();
        if(result == -1){
            return false;
        }else{
            return true;
        }
    }

    public void getData(){
        SQLiteDatabase db = App.getDbConnection().getReadableDatabaseInstance();

        ContentValues contentValues = new ContentValues();

    }

    public  boolean checkFlag(){
        SQLiteDatabase db = App.getDbConnection().getReadableDatabaseInstance();
        Cursor cursor = db.rawQuery("SELECT FLAG FROM "+ TABLE_NAME + " WHERE id = 0 ;", null);

        if((cursor != null) && cursor.moveToFirst()) {
            Log.d("DBhelper",cursor.getString(cursor.getColumnIndex("FLAG")));

            if (cursor.getString(cursor.getColumnIndex("FLAG")).equals("1")) {
                Log.d("DBhelper",cursor.getString(cursor.getColumnIndex("FLAG")));
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public String getClient(){
        SQLiteDatabase db = App.getDbConnection().getReadableDatabaseInstance();
        Cursor cursor = db.rawQuery("SELECT EMAIL FROM "+ TABLE_NAME + " WHERE id = 0 ;", null);
        String client = "";
        if((cursor != null) && cursor.moveToFirst()) {
            client = cursor.getString(cursor.getColumnIndex(EMAIL));
        }
        db.close();
        return client;
    }

    public  String getPasscode(){
        SQLiteDatabase db = App.getDbConnection().getReadableDatabaseInstance();
        Cursor cursor = db.rawQuery("SELECT PASSCODE FROM "+ TABLE_NAME + " WHERE id = 0;", null);
        String client = "";
        if((cursor != null) && cursor.moveToFirst()) {
            client = cursor.getString(cursor.getColumnIndex(PASSCODE));
        }
        db.close();
        return client;
    }

    public String getAuthcode(){
        SQLiteDatabase db = App.getDbConnection().getReadableDatabaseInstance();
        Cursor cursor = db.rawQuery("SELECT AUTHCODE FROM "+ TABLE_NAME + " WHERE id = 0 ;", null);
        String client = "";
        if((cursor != null) && cursor.moveToFirst()) {
            client = cursor.getString(cursor.getColumnIndex(AUTHCODE));
        }
        db.close();
        return client;
    }
}
