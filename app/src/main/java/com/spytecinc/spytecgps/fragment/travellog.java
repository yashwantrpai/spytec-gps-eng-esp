package com.spytecinc.spytecgps.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.spytecinc.spytecgps.CreatePostString;
import com.spytecinc.spytecgps.db.DatabaseHelper;
import com.spytecinc.spytecgps.R;
import com.spytecinc.spytecgps.activity.HomeActivity;
import com.spytecinc.spytecgps.activity.SplashScreen;
import com.spytecinc.spytecgps.util.Util;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class travellog extends Fragment {

    public travellog() {
        // Required empty public constructor
    }

    public static Context context;
    DatabaseHelper dbHelper;
    String TAG = "Travel Log";
    WebView wv1;
    public SharedPreferences pref;
    public SharedPreferences.Editor editor;
    public ProgressBar progress;

    public static TextView travellog_title;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.travellog, container, false);
        context = this.getContext();
        dbHelper = new DatabaseHelper();

        pref = context.getSharedPreferences("DataStore", MODE_PRIVATE);
        editor = pref.edit();

        travellog_title = (TextView) rootView.findViewById(R.id.travellog_title);
        travellog_title.setText((pref.getString("language", "0").equals("0"))?R.string.travellog:R.string.travellog_esp);

        if(pref.getString("curfile", "") == null || !HomeActivity.equalStrings(pref.getString("curfile", "").toString(), "travellog")) {
            editor.putString("curfile", "travellog");
            editor.commit();
        }

        // String[] startDatesplit = String.valueOf(travellogform.travellog_values.get(0)).split("/");
        // String[] endDatesplit = String.valueOf(travellogform.travellog_values.get(1)).split("/");

        // String start_date = String.valueOf(travellogform.travellog_values.get(0));
        String start_date = pref.getString("startdate", "");
        // String end_date = String.valueOf(travellogform.travellog_values.get(1));
        String end_date = pref.getString("enddate", "");
        // String datapoint = String.valueOf(travellogform.travellog_values.get(2));
        String datapoint = pref.getString("datapoint", "");
        // String driver = String.valueOf(travellogform.travellog_values.get(3));
        String driver = pref.getString("driver", "");
        String client = dbHelper.getClient();

        // Back button
        ImageView back_btn = (ImageView) rootView.findViewById(R.id.back);
        back_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Fragment reportsFragment = new com.spytecinc.spytecgps.fragment.travellogform();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                editor.putString("CURRENT_TAG", HomeActivity.TAG_TRAVELLOG_FORM);
                editor.commit();
                fragmentTransaction.replace(R.id.frame, reportsFragment, HomeActivity.TAG_TRAVELLOG_FORM);
                fragmentTransaction.commitAllowingStateLoss();
            }
        });

        // Log.i(TAG,"Entered values "+ start_date+" "+end_date+" "+" "+datapoint+" "+driver+" "+client);

        if(datapoint.equals("STOPS ONLY")){
            datapoint=datapoint.replace(" ","");
        }
        else if(datapoint.equals("IGNITION ON/OFF")){
            datapoint="IGNITION";
        }
        else  if(datapoint.equals("MOVEMENTS ONLY")){
            datapoint="MOVEMENT";
        }

        if(driver != "" && start_date != "" && end_date != "" && datapoint != "" && client != "") {
            String currentServer = pref.getString("currentServer", "");
            String urls[] = getResources().getStringArray(R.array.url);
            if(Util.isValidUrl(currentServer, urls)) {
                String url = currentServer + ((pref.getString("language", "0").equals("0"))?getString(R.string.travel_log_url):getString(R.string.travel_log_url_esp));
                ArrayList params = new ArrayList();
                Pair pair = new Pair("operation", "travellogreport");
                params.add(pair);
                pair = new Pair("driver", driver.replace(" ", ""));
                params.add(pair);
                pair = new Pair("startdate", start_date);
                params.add(pair);
                pair = new Pair("enddate", end_date);
                params.add(pair);
                pair = new Pair("datapoint", datapoint);
                params.add(pair);
                pair = new Pair("authcode", dbHelper.getAuthcode());
                params.add(pair);
                String postString = "payload=" + CreatePostString.createPostString(params);

                Log.i("url", url);
                Log.i("param", postString);
                wv1 = (WebView) rootView.findViewById(R.id.webView);
                wv1.setWebViewClient(new MyBrowser());
                progress = (ProgressBar) rootView.findViewById(R.id.progressBar);

                wv1.getSettings().setLoadWithOverviewMode(true);
                wv1.getSettings().setUseWideViewPort(true);
                wv1.getSettings().setLoadsImagesAutomatically(true);
                wv1.getSettings().setJavaScriptEnabled(true);
                wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                wv1.postUrl(url, postString.getBytes());
            } else {
                Toast.makeText(context, (pref.getString("language", "0").equals("0"))?R.string.server_connection_issue:R.string.server_connection_issue_esp, Toast.LENGTH_SHORT).show();
                Intent i = new Intent(context, SplashScreen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        } else {
            Log.d(TAG, "Input values not inserted properly in Travel Log Report");
        }

        return rootView;
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progress.setVisibility(View.GONE);
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            progress.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);
        }
    }
}
