package com.spytecinc.spytecgps.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.content.Context;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.spytecinc.spytecgps.CreatePostString;
import com.spytecinc.spytecgps.CustomHttpClient;
// import com.spytecinc.spytecgps.FetchAddressIntentService;
import com.spytecinc.spytecgps.db.DatabaseHelper;
import com.spytecinc.spytecgps.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.spytecinc.spytecgps.activity.HomeActivity;
import com.spytecinc.spytecgps.activity.SplashScreen;
import com.spytecinc.spytecgps.util.Util;

import java.util.ArrayList;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class home extends Fragment implements GoogleMap.OnMapLoadedCallback, OnMapReadyCallback {

    public home() {
        // Required empty public constructor
    }

    MapView mapView;
    public static View rootView;
    private static final String TAG = "Maps Acitivty";
    public static String response;
    public static Button rb_normal, rb_satellite, rb_hybrid;
    public static GoogleMap googleMap;
    public static Marker marker_list[], current_marker;
    public static Double lat[], lng[];
    public static String Address="", Battery[], LastRead[], Name[], Speed[], Event[], Direction[];
    public static ImageView zoomplus, zoomminus, tvClose, batteryLevel;
    public static int num_drivers = 0, current_ind = 0;
    private LatLngBounds.Builder builder;
    private LatLngBounds bounds;
    private boolean isDestroyed = false;
    public SharedPreferences pref;
    public SharedPreferences.Editor editor;
    DatabaseHelper credentialStore;
    public String authcode;
    protected Location mLastLocation;
    public Context thisContext;
    public LinearLayout infowindowlayout;
    public TextView tvDate, tvBattery, tvAddress, tvName, tvMapIt, tvSpeed, tvEvent, tvDirection;
    public static String google_maps_api;
    public static ScheduledThreadPoolExecutor scheduledThreadPoolExecutor;
    public static LinearLayout progressLayout;
    public static ProgressBar progressBar;
    public static TextView progressText;

    public void loadAddress() {
        if(getActivity() != null) {
            if (!getActivity().isFinishing()) {
                ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                    // String url = "http://maps.google.com/maps/geo?q=" + Double.toString(lat[current_ind]) + "," + Double.toString(lng[current_ind]) + "&output=xml&oe=utf8&sensor=true&key=" + R.string.googlemaps_api_key;
                    // final String url = "http://nominatim-load-1807280891.us-east-1.elb.amazonaws.com/reverse_geocode_tracking.php?lat=" + lat[current_ind] + "&lng=" + lng[current_ind];
                    // Log.i("--url--", url);
                    Address = "";
                    try {
                        String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + Double.toString(lat[current_ind]) + "," + Double.toString(lng[current_ind]) + "&key=" + google_maps_api;
                        getGeocodeAddress getGeocodeAddress = new getGeocodeAddress();
                        getGeocodeAddress.execute(url);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getContext(), (pref.getString("language", "0").equals("0"))?R.string.internet_connectivity_check:R.string.internet_connectivity_check_esp, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public Bitmap resizeMapIcons(String iconName, int width, int height) {
        Bitmap imageBitmap = null;
        if(isAdded()) {
            try {
                imageBitmap = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(iconName, "drawable", getContext().getPackageName()));
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        if (imageBitmap != null) {
            try {
                Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, width, height, false);
                return resizedBitmap;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return imageBitmap;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.map_fragment, container, false);
        isDestroyed = false;
        pref = getContext().getSharedPreferences("DataStore", MODE_PRIVATE);
        editor = pref.edit();

        rb_normal = (Button) rootView.findViewById(R.id.rb_normal);
        rb_normal.setText((pref.getString("language", "0").equals("0"))?R.string.standard_view:R.string.standard_view_esp);
        rb_satellite = (Button) rootView.findViewById(R.id.rb_satellite);
        rb_satellite.setText((pref.getString("language", "0").equals("0"))?R.string.satellite_view:R.string.satellite_view_esp);
        rb_hybrid = (Button) rootView.findViewById(R.id.rb_hybrid);
        rb_hybrid.setText((pref.getString("language", "0").equals("0"))?R.string.hybrid_view:R.string.hybrid_view_esp);

        zoomminus = (ImageView) rootView.findViewById(R.id.zoomminus);
        zoomplus = (ImageView) rootView.findViewById(R.id.zoomplus);

        progressLayout = (LinearLayout) getActivity().findViewById(R.id.progressLayout);
        progressBar = (ProgressBar) getActivity().findViewById(R.id.progressbar);
        progressText = (TextView) getActivity().findViewById(R.id.progressText);

        infowindowlayout = (LinearLayout) rootView.findViewById(R.id.infowindowlayout);
        tvDate = (TextView) rootView.findViewById(R.id.tv_lastread);
        tvBattery = (TextView) rootView.findViewById(R.id.tv_battery);
        tvAddress = (TextView) rootView.findViewById(R.id.tv_address);
        tvName = (TextView) rootView.findViewById(R.id.tv_name);
        tvMapIt = (TextView) rootView.findViewById(R.id.tv_mapit);
        tvSpeed = (TextView) rootView.findViewById(R.id.tv_speed);
        tvEvent = (TextView) rootView.findViewById(R.id.tv_event);
        tvDirection = (TextView) rootView.findViewById(R.id.tv_direction);
        tvClose = (ImageView) rootView.findViewById(R.id.tv_close);
        batteryLevel = (ImageView) rootView.findViewById(R.id.battery_level);
        google_maps_api = getResources().getString(R.string.googlemaps_api_key);

        credentialStore = new DatabaseHelper();
        authcode = credentialStore.getAuthcode();

        if(pref.getString("curfile", "") == null || !HomeActivity.equalStrings(pref.getString("curfile", "").toString(), "home")) {
            editor.putString("curfile", "home");
            editor.commit();
        }

        ImageView refresh_btn = (ImageView) rootView.findViewById(R.id.restart);
        refresh_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Reload current fragment
                if(getActivity() != null) {
                    if (!getActivity().isFinishing()) {
                        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                            currentlocation();
                        } else {
                            Toast.makeText(getContext(), (pref.getString("language", "0").equals("0"))?R.string.internet_connectivity_check:R.string.internet_connectivity_check_esp, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });

        return rootView;
    }

    public void currentlocation() {
        String currentServer = pref.getString("currentServer", "");
        String urls[] = getActivity().getResources().getStringArray(R.array.url);
        if(Util.isValidUrl(currentServer, urls)) {
            String currentLocationUrl = currentServer + ((pref.getString("language", "0").equals("0"))?getString(R.string.current_location_url):getString(R.string.current_location_url_esp));
            ArrayList params = new ArrayList();
            Pair pair = new Pair("operation", "currentlocation");
            params.add(pair);
            pair = new Pair("authcode", authcode);
            params.add(pair);

            String postString = CreatePostString.createPostString(params);
            // Log.i("url", currentLocationUrl);
            // Log.i("param", postString);
            getCurrentLocation currentLocationTask = new getCurrentLocation();
            currentLocationTask.execute(currentLocationUrl, postString);
        } else {
            Toast.makeText(getContext(), "Server connectivity issue..", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(getActivity(), SplashScreen.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        thisContext = context;
    }

    public SpannableStringBuilder getSpannedText(String str, int startInd, int endInd){
        final SpannableStringBuilder sb = new SpannableStringBuilder(str);

        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        sb.setSpan(bss, startInd, endInd, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        return sb;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        isDestroyed = false;

        mapView = (com.google.android.gms.maps.MapView) rootView.findViewById(R.id.mapView);
        try {
            if (mapView != null) {
                mapView.onCreate(savedInstanceState);
            }
            mapView.onResume(); // needed for getting the map to display immediately
        } catch (Exception e){
            e.printStackTrace();
        }

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap mMap) {
                googleMap = mMap;
                googleMap.setIndoorEnabled(false);
                googleMap.getUiSettings().setScrollGesturesEnabled(true);
                googleMap.getUiSettings().setZoomGesturesEnabled(true);
                googleMap.getUiSettings().setRotateGesturesEnabled(true);

                switch (pref.getString("mapSelect",""))
                {
                    case "Normal":
                        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                        rb_normal.setSelected(true);
                        rb_satellite.setSelected(false);
                        rb_hybrid.setSelected(false);
                        editor.putString("mapSelect", "Normal");
                        editor.commit();
                        break;

                    case "Satellite":
                        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                        rb_satellite.setSelected(true);
                        rb_normal.setSelected(false);
                        rb_hybrid.setSelected(false);
                        editor.putString("mapSelect", "Satellite");
                        editor.commit();
                        break;

                    case "Hybrid":
                        googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                        rb_hybrid.setSelected(true);
                        rb_normal.setSelected(false);
                        rb_satellite.setSelected(false);
                        editor.putString("mapSelect", "Hybrid");
                        editor.commit();
                        break;

                    default:
                        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                        rb_normal.setSelected(true);
                        rb_satellite.setSelected(false);
                        rb_hybrid.setSelected(false);
                        editor.putString("mapSelect", "Normal");
                        editor.commit();
                }
                // googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

                if(HomeActivity.equalStrings(pref.getString("curfile", "").toString(), "home")) {
                    if (HomeActivity.equalStrings(pref.getString("mapSelect", "").toString(), "Normal")) {
                        rb_normal.setSelected(true);
                    } else if(HomeActivity.equalStrings(pref.getString("mapSelect", "").toString(), "Satellite")){
                        rb_satellite.setSelected(true);
                    } else if(HomeActivity.equalStrings(pref.getString("mapSelect", "").toString(), "Hybrid")){
                        rb_hybrid.setSelected(true);
                    } else {
                        rb_normal.setSelected(true);
                    }
                } else {
                    rb_normal.setSelected(true);
                }

                rb_normal.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                        rb_normal.setSelected(true);
                        rb_satellite.setSelected(false);
                        rb_hybrid.setSelected(false);

                        editor.putString("mapSelect", "Normal");
                        editor.commit();
                        return true;
                    }
                });

                rb_satellite.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                        rb_satellite.setSelected(true);
                        rb_normal.setSelected(false);
                        rb_hybrid.setSelected(false);

                        editor.putString("mapSelect", "Satellite");
                        editor.commit();
                        return true;
                    }
                });

                rb_hybrid.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                        rb_hybrid.setSelected(true);
                        rb_normal.setSelected(false);
                        rb_satellite.setSelected(false);

                        editor.putString("mapSelect", "Hybrid");
                        editor.commit();
                        return true;
                    }
                });

                googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        if(getActivity() != null) {
                            if (!getActivity().isFinishing()) {
                                ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                                if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                                    currentlocation();
                                } else {
                                    Toast.makeText(getContext(), (pref.getString("language", "0").equals("0"))?R.string.internet_connectivity_check:R.string.internet_connectivity_check_esp, Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                });

                zoomplus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(googleMap != null) {
                            float newzoom = googleMap.getCameraPosition().zoom + 1;
                            LatLng centerLatLng = googleMap.getCameraPosition().target;
                            Double latitude = centerLatLng.latitude;
                            Double longitude = centerLatLng.longitude;
                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), newzoom));
                        }
                    }
                });

                zoomminus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(googleMap != null) {
                            float newzoom = googleMap.getCameraPosition().zoom - 1;
                            LatLng centerLatLng = googleMap.getCameraPosition().target;
                            Double latitude = centerLatLng.latitude;
                            Double longitude = centerLatLng.longitude;
                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), newzoom));
                        }
                    }
                });
            }
        });
    }



    public void loadMap(){
        if (googleMap == null) {
            if(getActivity() != null) {
                if (!getActivity().isFinishing()) {
                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialogBuilder.setMessage("Google map required full reload. Navigate to a different page and come back to this page ");
                    alertDialogBuilder.setNeutralButton(this.getResources().getString((pref.getString("language", "0").equals("0"))?R.string.ok:R.string.ok_esp), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            arg0.dismiss();
                        }
                    });
                    alertDialogBuilder.show();
                }
            }
        } else {
            googleMap.clear();
            if (isDataPresent()) {
                if (lat.length > 0 && lng.length > 0) {
                    marker_list = new Marker[num_drivers];
                    builder = new LatLngBounds.Builder();
                    boolean markersIncluded = false;
                    if (marker_list != null) {
                        for (int i = 0; i < marker_list.length; i++) {
                            try {
                                if ((lat[i] != null) && (lng[i] != null)) {
                                    marker_list[i] = googleMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(lat[i], lng[i]))
                                            .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("markericon", 90, 90))));
                                    if (marker_list[i] != null) {
                                        builder.include(marker_list[i].getPosition());
                                        markersIncluded = true;
                                    } else {
                                        Log.w(TAG, "marker_list is null at index " + i);
                                    }
                                }
                            } catch(Exception e){
                                e.printStackTrace();
                            }
                        }
                    } else {
                        Log.w(TAG, "marker_list is null");
                    }
                    //If marker is not included it causes IllegalStateException
                    if (markersIncluded) {
                        bounds = builder.build();
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 80));
                        float currentzoom = googleMap.getCameraPosition().zoom;
                        if (currentzoom > 14) {
                            Double lat_avg = 0.0, lng_avg = 0.0, lat_sum = 0.0, lng_sum = 0.0;
                            for (int i = 0; i < lat.length; i++) {
                                lat_sum += lat[i];
                                lng_sum += lng[i];
                            }
                            lat_avg = (lat_sum / lat.length);
                            lng_avg = (lng_sum / lng.length);
                            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat_avg, lng_avg), 14));
                        }
                    }

                    // Setting a custom info window adapter for the google map
                    // googleMap.setInfoWindowAdapter(new CustomInfoWindowAdapter());

                    googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {
                            for (int i = 0; i < marker_list.length; i++) {
                                if (marker_list[i] != null) {
                                    if (marker_list[i].getPosition().latitude == marker.getPosition().latitude && marker_list[i].getPosition().longitude == marker.getPosition().longitude) {
                                        current_marker = marker;
                                        current_ind = i;

                                        LatLngBounds.Builder markerbuilder;
                                        LatLngBounds markerbounds;
                                        markerbuilder = new LatLngBounds.Builder();
                                        markerbuilder.include(marker.getPosition());
                                        markerbounds = markerbuilder.build();
                                        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(markerbounds, 60));
                                        float currentzoom = googleMap.getCameraPosition().zoom;
                                        if (currentzoom > 15) {
                                            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(marker.getPosition().latitude, marker.getPosition().longitude), 13));
                                        }
                                        break;
                                    }
                                } else {
                                    Log.w(TAG, "marker_list is null at index " + i);
                                }
                            }
                            loadAddress();
                            return true;
                        }
                    });

                    googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        @Override
                        public void onInfoWindowClick(Marker marker) {

                        }
                    });
                } else {
                    if(getActivity() != null) {
                        if(!getActivity().isFinishing()) {
                            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            final AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialogBuilder.setMessage((pref.getString("language", "0").equals("0"))?R.string.no_data_available:R.string.no_data_available_esp);
                            alertDialogBuilder.setNeutralButton(getActivity().getResources().getString((pref.getString("language", "0").equals("0"))?R.string.ok:R.string.ok_esp), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    arg0.dismiss();
                                }
                            });
                            alertDialogBuilder.show();
                        }
                    }
                }
            } else {
                if(getActivity() != null) {
                    if (!getActivity().isFinishing()) {
                        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                        final AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialogBuilder.setTitle((pref.getString("language", "0").equals("0"))?R.string.data_sync_issue:R.string.data_sync_issue_esp);
                        alertDialogBuilder.setMessage((pref.getString("language", "0").equals("0"))?R.string.re_login_and_try_again:R.string.re_login_and_try_again_esp);
                        alertDialogBuilder.setNeutralButton(getActivity().getResources().getString((pref.getString("language", "0").equals("0"))?R.string.ok:R.string.ok_esp),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialogBuilder.show();
                    }
                }
            }
        }
    }

    public Boolean isDataPresent(){
        if (lat != null && lng != null && Name != null && Battery != null && LastRead != null){
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        try {
            if (mapView != null) {
                mapView.onSaveInstanceState(outState);
            } else {
                Log.w(TAG, "MapView is null");
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (mapView != null) {
                mapView.onResume();
            } else {
                Log.w(TAG, "MapView is null");
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(1);
        scheduledThreadPoolExecutor.scheduleAtFixedRate(TimerTask, 60, 60, TimeUnit.SECONDS);

        dismissProgressDialog();
        closeInfoWindowLayout();

        isDestroyed = false;
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            if (mapView != null) {
                mapView.onPause();
            } else {
                Log.w(TAG, "MapView is null");
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        if (scheduledThreadPoolExecutor != null) {
        Log.i("lc", "timer shutdown");
            scheduledThreadPoolExecutor.shutdown();
        }
        dismissProgressDialog();
        closeInfoWindowLayout();
        // logout();

        isDestroyed = false;
    }

    @Override
    public void onDestroy() {
        isDestroyed = true;
        dismissProgressDialog();
        closeInfoWindowLayout();
        super.onDestroy();
        try {
            if (mapView != null) {
                mapView.onDestroy();
            } else {
                Log.w(TAG, "MapView is null");
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        // logout();
    }

    @Override
    public void onStart() {
        super.onStart();
        scheduledThreadPoolExecutor=new ScheduledThreadPoolExecutor(1);
        scheduledThreadPoolExecutor.scheduleAtFixedRate(TimerTask, 60, 60, TimeUnit.SECONDS);

        isDestroyed = false;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (scheduledThreadPoolExecutor != null) {
            scheduledThreadPoolExecutor.shutdown();
        }

        dismissProgressDialog();
        closeInfoWindowLayout();

        isDestroyed = false;
        // logout();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        try {
            if (mapView != null) {
                mapView.onLowMemory();
            } else {
                Log.w(TAG, "MapView is null");
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        dismissProgressDialog();
        closeInfoWindowLayout();
    }

    private void dismissProgressDialog() {
        if(progressLayout.getVisibility() == View.VISIBLE)
        {
            progressText.setText("");
            progressLayout.setVisibility(View.GONE);
            if(googleMap != null) {
                googleMap.getUiSettings().setZoomGesturesEnabled(true);
                googleMap.getUiSettings().setRotateGesturesEnabled(true);
                googleMap.getUiSettings().setScrollGesturesEnabled(true);
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
    }

    @Override
    public void onMapLoaded() {
        if(getActivity() != null) {
            if (!getActivity().isFinishing()) {
                ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                    currentlocation();
                } else {
                    Toast.makeText(getContext(), (pref.getString("language", "0").equals("0"))?R.string.internet_connectivity_check:R.string.internet_connectivity_check_esp, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    Runnable TimerTask = new Runnable() {
        @Override
        public void run() {
            if (getActivity() != null) {
                if (!getActivity().isFinishing()) {
                    ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                    if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                currentlocation();
                            }
                        });
                    } else {
                        getActivity().runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(getContext(), (pref.getString("language", "0").equals("0"))?R.string.internet_connectivity_check:R.string.internet_connectivity_check_esp, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }
        }
    };

    private class getGeocodeAddress extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            // TODO Auto-generated method stub
            String res = null;
            try {
                response = CustomHttpClient.executeHttpPost(url[0]);
                res = response.toString();
                //res= res.replaceAll("\\s+","");
            } catch (Exception e) {
                e.printStackTrace();
            }

            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            if(result != null){
                if(result != ""){
                    try {
                        JSONObject addrObject = new JSONObject(result);
                        JSONArray addressArray = addrObject.getJSONArray("results");
                        if (addressArray != null) {
                            if (addressArray.length() > 0) {
                                JSONObject addressObj = addressArray.getJSONObject(0);
                                String addr = "";
                                addr = addressObj.get("formatted_address").toString();
                                if (addr != null && addr != "") {
                                    Address = addr;
                                } else {
                                    String address_components_str = addressObj.get("address_components").toString();
                                    JSONArray address_components_arr = new JSONArray(address_components_str);
                                    if(address_components_arr != null){
                                        for(int i = 0; i < address_components_arr.length(); i++){
                                            try {
                                                JSONObject obj = address_components_arr.getJSONObject(i);
                                                if (obj != null) {
                                                    try {
                                                        String component = obj.get("long_name").toString();
                                                        if (component != null) {
                                                            if(i == 0){
                                                                addr += component;
                                                            } else {
                                                                addr += ", " + component;
                                                            }
                                                        }
                                                    } catch (Exception e){
                                                        e.printStackTrace();
                                                    }
                                                }
                                            } catch (Exception e){
                                                e.printStackTrace();
                                            }
                                        }

                                        if(addr != null && addr != ""){
                                            Address = addr;
                                        }
                                    }
                                }
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            if(Address != null && Address != "") {
                /* if (current_marker != null) {
                    current_marker.showInfoWindow();
                } else {
                    marker_list[current_ind].showInfoWindow();
                } */
                showInfoWindow();
            } else {
                getLocationAndroidGeocoder getLocationAndroidGeocoder = new getLocationAndroidGeocoder();
                getLocationAndroidGeocoder.execute();
            }
        }
    }

    private class getLocationAndroidGeocoder extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            // TODO Auto-generated method stub
            String res = null;
            String address_full = "";

            if(thisContext != null) {
                Geocoder geoCoder = new Geocoder(thisContext);
                java.util.List<android.location.Address> matches = null;
                try {
                    double latitude = lat[current_ind].doubleValue();
                    double longitude = lng[current_ind].doubleValue();
                    matches = geoCoder.getFromLocation(latitude, longitude, 1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                android.location.Address address = null;
                if (matches != null) {
                    address = (matches.isEmpty() ? null : matches.get(0));
                }

                if (address != null) {
                    if (address.getAddressLine(0) == null) {
                        address_full += (address.getLocality() == null) ? "" : address.getLocality() + " ";
                        address_full += (address.getAdminArea() == null) ? "" : address.getAdminArea() + " ";
                        address_full += (address.getCountryCode() == null) ? "" : address.getCountryCode();
                    } else {
                        address_full = address.getAddressLine(0);
                    }
                }
            }

            return address_full;

        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            if(result != null && result != "") {
                Address = result;
            }

            /* if(current_marker != null) {
                current_marker.showInfoWindow();
            }else{
                marker_list[current_ind].showInfoWindow();
            } */
            showInfoWindow();
        }
    }

    public class getCurrentLocation extends AsyncTask<String, Void, String> implements DialogInterface.OnCancelListener{

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            if(!isDestroyed) {
                progressLayout.setVisibility(View.VISIBLE);
                progressText.setText(pref.getString("language", "0").equals("0")?R.string.please_wait:R.string.please_wait_esp);
            }
        }

        @Override
        protected String doInBackground(String... url) {
            // TODO Auto-generated method stub
            String res = null;
            try {
                response = CustomHttpClient.executeHttpPost(url[0], url[1]);
                res = response.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return res;
        }//close doInBackground

        @Override
        protected void onPostExecute(String result) {
            Boolean responsefailed = false;
            dismissProgressDialog();
            closeInfoWindowLayout();

            if (!isDestroyed) {
                response = result;

                if (response != null && response != "") {
                    try {
                        JSONObject respjson = new JSONObject(response);
                        if (respjson.get("status").equals("true")) {
                            JSONObject jsondata = new JSONObject();
                            jsondata = respjson.getJSONObject("data");
                            JSONArray driverarray = jsondata.getJSONArray("driverdata");
                            // Log.d("-------------",driverarray.length()+" ");
                            num_drivers = driverarray.length();
                            Name = new String[num_drivers];
                            lat = new Double[num_drivers];
                            lng = new Double[num_drivers];
                            LastRead = new String[num_drivers];
                            Battery = new String[num_drivers];
                            Speed = new String[num_drivers];
                            Direction = new String[num_drivers];
                            Event = new String[num_drivers];

                            JSONObject driverobj = new JSONObject();
                            for (int i = 0; i < num_drivers; i++) {
                                driverobj = driverarray.getJSONObject(i);
                                Name[i] = driverobj.get("drivername").toString();
                                lat[i] = Double.parseDouble(driverobj.get("lat").toString());
                                lng[i] = Double.parseDouble(driverobj.get("long").toString());
                                LastRead[i] = driverobj.get("active").toString();
                                Battery[i] = driverobj.get("batterylevel").toString();
                                Speed[i] = driverobj.get("speed").toString();
                                Direction[i] = driverobj.get("direction").toString();
                                Event[i] = driverobj.get("event").toString();
                            }
                        } else {
                            if(getActivity() != null) {
                                if(!getActivity().isFinishing()) {
                                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                                    final AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialogBuilder.setTitle((pref.getString("language", "0").equals("0"))?R.string.authentication_error:R.string.authentication_error_esp);
                                    alertDialogBuilder.setMessage((pref.getString("language", "0").equals("0"))?R.string.re_login:R.string.re_login_esp);
                                    alertDialogBuilder.setNeutralButton(getActivity().getResources().getString((pref.getString("language", "0").equals("0"))?R.string.ok:R.string.ok_esp),
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    JSONObject data = new JSONObject();
                                                    try {
                                                        boolean isInserted = credentialStore.setFlag(0, "", "", "");
                                                        dialog.dismiss();
                                                        if(getActivity() != null){
                                                            if(!getActivity().isFinishing()) {
                                                                Intent i = new Intent(getActivity(), SplashScreen.class);
                                                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                                startActivity(i);
                                                            }
                                                        }
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                        dialog.dismiss();
                                                    }
                                                }
                                            });
                                    alertDialogBuilder.show();
                                }}
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        responsefailed = true;
                    }
                }else{
                    responsefailed = true;
                }

                if(responsefailed){
                    if(getActivity() != null) {
                        if (!getActivity().isFinishing()) {
                            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            final AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialogBuilder.setTitle((pref.getString("language", "0").equals("0"))?R.string.data_sync_issue:R.string.data_sync_issue_esp);
                            alertDialogBuilder.setMessage((pref.getString("language", "0").equals("0"))?R.string.re_login_and_try_again:R.string.re_login_and_try_again_esp);
                            alertDialogBuilder.setPositiveButton((pref.getString("language", "0").equals("0"))?R.string.ok:R.string.ok_esp,
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialogBuilder.show();
                        }
                    }
                } else if(num_drivers == 0){
                    if(getActivity() != null) {
                        if (!getActivity().isFinishing()) {
                            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
                            final AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialogBuilder.setMessage((pref.getString("language", "0").equals("0"))?R.string.no_data_available:R.string.no_data_available_esp);
                            alertDialogBuilder.setNeutralButton(getActivity().getResources().getString((pref.getString("language", "0").equals("0"))?R.string.ok:R.string.ok_esp), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    arg0.dismiss();
                                }
                            });
                            alertDialogBuilder.show();
                        }
                    }
                }else {
                    editor.putString("mapSelect", "Normal");
                    editor.commit();
                    try {
                        editor.putString("drivers", new JSONArray(Name).toString());
                        editor.apply();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    loadMap();
                }
            }
        }

        @Override
        public void onCancel(DialogInterface dialog) {
            cancel(true);
        }
    }

    public void closeInfoWindowLayout(){
        infowindowlayout.setVisibility(View.INVISIBLE);
        if(googleMap != null) {
            googleMap.getUiSettings().setZoomGesturesEnabled(true);
            googleMap.getUiSettings().setRotateGesturesEnabled(true);
            googleMap.getUiSettings().setScrollGesturesEnabled(true);
            if(bounds != null) {
                googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 80));
            }
            float currentzoom = googleMap.getCameraPosition().zoom;
            if (currentzoom > 13) {
                if(lat != null && lng != null) {
                    Double lat_avg = 0.0, lng_avg = 0.0, lat_sum = 0.0, lng_sum = 0.0;
                    for (int i = 0; i < lat.length; i++) {
                        lat_sum += lat[i];
                        lng_sum += lng[i];
                    }
                    lat_avg = (lat_sum / lat.length);
                    lng_avg = (lng_sum / lng.length);
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat_avg, lng_avg), 13));
                }
            }
        }
    }

    public void showInfoWindow(){
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeInfoWindowLayout();
            }
        });

        if(Name[current_ind] != null) {
            String drivername = Name[current_ind];
            tvName.setText(drivername);
        } else {
            Log.w(TAG, "Name value null for index"+ current_ind);
        }

        if(Speed[current_ind] != null) {
            String speedStr = "Speed: " + Speed[current_ind]  + " mi/h";
            SpannableStringBuilder spannedStr = getSpannedText(speedStr, 0, 5);
            tvSpeed.setText(spannedStr);
        } else {
            Log.w(TAG, "Speed value null for index"+ current_ind);
        }

        if(Direction[current_ind] != null) {
            String directionStr = "Dir.: " + Direction[current_ind];
            SpannableStringBuilder spannedStr = getSpannedText(directionStr, 0, 4);
            tvDirection.setText(spannedStr);
        } else {
            Log.w(TAG, "Direction value null for index"+ current_ind);
        }

        if(Event[current_ind] != null) {
            String eventStr = "Event: " + Event[current_ind];
            SpannableStringBuilder spannedStr = getSpannedText(eventStr, 0, 5);
            tvEvent.setText(spannedStr);
        } else {
            Log.w(TAG, "Event value null for index"+ current_ind);
        }

        if(LastRead[current_ind] != null) {
            String currentStr = "Last read: " + LastRead[current_ind];
            if(pref.getString("language", "0").equals("1")) {
                currentStr = currentStr.replace("days", "dias");
                currentStr = currentStr.replace("minutes", "minutos");
                currentStr = currentStr.replace("ago", "hace");
            }
            SpannableStringBuilder spannedStr = getSpannedText(currentStr, 0, 9);
            tvDate.setText(spannedStr);
        } else {
            Log.w(TAG, "LastRead value null for index"+ current_ind);
        }

        int currentOrientation = getResources().getConfiguration().orientation;
        if (Battery[current_ind] != null) {
            tvBattery.setText(Battery[current_ind] + "%");
            if (Double.parseDouble(Battery[current_ind]) == 0)
                batteryLevel.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.battery0, null));
            else if (Double.parseDouble(Battery[current_ind]) >= 0 && Double.parseDouble(Battery[current_ind]) < 17)
                batteryLevel.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.battery1, null));
            else if (Double.parseDouble(Battery[current_ind]) >= 17 && Double.parseDouble(Battery[current_ind]) < 34)
                batteryLevel.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.battery2, null));
            else if (Double.parseDouble(Battery[current_ind]) >= 34 && Double.parseDouble(Battery[current_ind]) < 51)
                batteryLevel.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.battery3, null));
            else if (Double.parseDouble(Battery[current_ind]) >= 51 && Double.parseDouble(Battery[current_ind]) < 68)
                batteryLevel.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.battery4, null));
            else if (Double.parseDouble(Battery[current_ind]) >= 68 && Double.parseDouble(Battery[current_ind]) < 85)
                batteryLevel.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.battery5, null));
            else if (Double.parseDouble(Battery[current_ind]) >= 85 && Double.parseDouble(Battery[current_ind]) < 97)
                batteryLevel.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.battery6, null));
            else if (Double.parseDouble(Battery[current_ind]) >= 97)
                batteryLevel.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.battery7, null));
            else
                batteryLevel.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.battery4, null));
        } else {
            Log.w(TAG, "Battery value null for index" + current_ind);
        }

        if (Address != null) {
            String addressStr = "Address: "+ Address;
            SpannableStringBuilder spannedStr = getSpannedText(addressStr, 0, 7);
            tvAddress.setText(spannedStr);
        } else {
            Log.w(TAG, "Address null for index" + current_ind);
        }

        tvMapIt.setText((pref.getString("language", "0").equals("0"))?getString(R.string.map_it):getString(R.string.map_it_esp));
        tvMapIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?q="+lat[current_ind]+","+lng[current_ind]));
                startActivity(browserIntent);
            }
        });

        infowindowlayout.setVisibility(View.VISIBLE);
        googleMap.getUiSettings().setZoomGesturesEnabled(false);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        googleMap.getUiSettings().setScrollGesturesEnabled(false);
    }
}
