package com.spytecinc.spytecgps.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.spytecinc.spytecgps.db.DatabaseHelper;
import com.spytecinc.spytecgps.R;
import com.spytecinc.spytecgps.activity.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class login extends Fragment {


    public login() {
        // Required empty public constructor
    }

    DatabaseHelper dbHelper;
    public static Context context;
    public static SharedPreferences pref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_main, container, false);
        context = this.getContext();
        dbHelper = new DatabaseHelper();
        pref = context.getSharedPreferences("DataStore", Context.MODE_PRIVATE);

        AlertDialog SignOutAlert = new AlertDialog.Builder(context).create();
        SignOutAlert.setTitle("Do you want to logout?");
        SignOutAlert.setButton(AlertDialog.BUTTON_POSITIVE, (pref.getString("language", "0").equals("0"))?context.getResources().getString(R.string.yes):context.getResources().getString(R.string.yes_esp),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        boolean isInserted = dbHelper.setFlag(0, "", "","");
                        Intent i = new Intent(context, MainActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                    }
                });

        SignOutAlert.setButton(AlertDialog.BUTTON_NEGATIVE, (pref.getString("language", "0").equals("0"))?context.getResources().getString(R.string.no):context.getResources().getString(R.string.no_esp),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        getFragmentManager().popBackStackImmediate();
                    }
                });
        SignOutAlert.show();

        return rootView;
    }
}
