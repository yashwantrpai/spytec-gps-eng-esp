package com.spytecinc.spytecgps.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.spytecinc.spytecgps.R;
import com.spytecinc.spytecgps.activity.HomeActivity;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class reports extends Fragment {

    ArrayList<Integer> Image_icon = new ArrayList<>();
    ArrayList<String> text = new ArrayList<>();
    ArrayList<Integer> Image = new ArrayList<>();
    RecyclerView Alert_recycler_View;
    ReportsAdapter Adapter;
    public SharedPreferences pref;
    public SharedPreferences.Editor editor;

    public reports() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.reports, container, false);

        pref = getContext().getSharedPreferences("DataStore", MODE_PRIVATE);
        editor = pref.edit();

        Image_icon.clear();
        text.clear();
        Image.clear();

        Alert_recycler_View = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        Adapter = new ReportsAdapter(Image_icon, text, Image);
        Alert_recycler_View.setAdapter(Adapter);
        Alert_recycler_View.setHasFixedSize(true);
        Alert_recycler_View.setLayoutManager(new LinearLayoutManager(getActivity()));

        setNewLayout(R.drawable.reporticon, getString((pref.getString("language", "0").equals("0"))?R.string.dtc:R.string.dtc_esp), R.drawable.next);
        setNewLayout(R.drawable.reporticon, getString((pref.getString("language", "0").equals("0"))?R.string.travellog:R.string.travellog_esp), R.drawable.next);
        setNewLayout(R.drawable.reporticon, getString((pref.getString("language", "0").equals("0"))?R.string.mileage:R.string.mileage_esp), R.drawable.next);
        setNewLayout(R.drawable.reporticon, getString((pref.getString("language", "0").equals("0"))?R.string.fuel:R.string.fuel_esp), R.drawable.next);

        return rootView;
    }

    public void setNewLayout(int image_icon, String txt, int image){
        Image_icon.add(image_icon);
        text.add(txt);
        Image.add(image);

        Adapter.notifyDataSetChanged();
    }

    public void goToFragment(int position){
        String CURRENT_TAG = HomeActivity.TAG_REPORTS;
        Fragment targetFragment = new reports();
        switch (position){
            case 0:
                targetFragment = new dtcform();
                editor.putString("CURRENT_TAG", HomeActivity.TAG_DTC_FORM);
                editor.commit();
                break;

            case 1:
                targetFragment = new travellogform();
                editor.putString("CURRENT_TAG", HomeActivity.TAG_TRAVELLOG_FORM);
                editor.commit();
                break;

            case 2:
                targetFragment = new mileageform();
                editor.putString("CURRENT_TAG", HomeActivity.TAG_MILEAGE_FORM);
                editor.commit();
                break;

            case 3:
                targetFragment = new fuelreportform();
                editor.putString("CURRENT_TAG", HomeActivity.TAG_FUELREPORT_FORM);
                editor.commit();
                break;

            default:
                targetFragment = new reports();
                editor.putString("CURRENT_TAG", HomeActivity.TAG_REPORTS);
                editor.commit();
                break;
        }

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.frame, targetFragment, CURRENT_TAG);
        fragmentTransaction.commitAllowingStateLoss();
    }

    public class ReportsAdapter extends RecyclerView.Adapter<ReportsAdapter.MyViewHolder> {

        ArrayList<Integer> Image_icon = new ArrayList<>();
        ArrayList<String> Text = new ArrayList<>();
        ArrayList<Integer> Image = new ArrayList<>();

        public ReportsAdapter(ArrayList<Integer> Image_icon, ArrayList<String> text, ArrayList<Integer> Image)
        {
            this.Image_icon = Image_icon;
            this.Text = text;
            this.Image = Image;
        }

        @Override
        public ReportsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View card_view = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewlayout, parent, false);
            return new ReportsAdapter.MyViewHolder(card_view);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position)
        {
            try {
                holder.text.setText(Text.get(position).toString());
                holder.image_icon.setImageResource(Image_icon.get(position));
                holder.image.setImageResource(Image.get(position));

                holder.image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        goToFragment(position);
                    }
                });
            } catch(Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return Image_icon.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView text;
            ImageView image_icon, image;

            public MyViewHolder(View view) {
                super(view);
                image_icon = (ImageView) view.findViewById(R.id.image_icon);
                text = (TextView) view.findViewById(R.id.text);
                image = (ImageView) view.findViewById(R.id.next);
            }
        }
    }
}
