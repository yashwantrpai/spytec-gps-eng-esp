package com.spytecinc.spytecgps.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.spytecinc.spytecgps.CreatePostString;
import com.spytecinc.spytecgps.CustomHttpClient;
import com.spytecinc.spytecgps.R;
import com.spytecinc.spytecgps.activity.HomeActivity;
import com.spytecinc.spytecgps.db.DatabaseHelper;

import static android.content.Context.MODE_PRIVATE;

import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class help extends Fragment {
    public SharedPreferences pref;
    public SharedPreferences.Editor editor;
    public Activity activity;
    ArrayList<Integer> Image_icon = new ArrayList<>();
    ArrayList<String> text = new ArrayList<>();
    ArrayList<Integer> Image = new ArrayList<>();
    RecyclerView Alert_recycler_View;
    SettingsAdapter Adapter;
    public AlertDialog.Builder selectLanguageAlert;
    public AlertDialog languageSelectDialog;
    public View languageSelectView;
    public RadioGroup radioGroup;
    public DatabaseHelper dbHelper;
    public boolean isLanguageModified = false;

    public help() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_help, container, false);
        pref = getContext().getSharedPreferences("DataStore", MODE_PRIVATE);
        editor = pref.edit();
        activity = getActivity();
        dbHelper = new DatabaseHelper();

        Image_icon.clear();
        text.clear();
        Image.clear();

        Alert_recycler_View = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        Adapter = new SettingsAdapter(Image_icon, text, Image);
        Alert_recycler_View.setAdapter(Adapter);
        Alert_recycler_View.setHasFixedSize(true);
        Alert_recycler_View.setLayoutManager(new LinearLayoutManager(getActivity()));

        setNewLayout(R.drawable.languageselect, getString((pref.getString("language", "0").equals("0"))?R.string.change_language:R.string.change_language_esp), R.drawable.next);
        setNewLayout(R.drawable.mailus, getString((pref.getString("language", "0").equals("0"))?R.string.contact_us:R.string.contact_us_esp), R.drawable.next);
        // setNewLayout(R.drawable.reporticon, "Fuel Report", R.drawable.next);

        return rootView;
    }

    public void sendMail(){
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto","support.gps@spytecgps3.net", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Spytec GPS Support mail");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        startActivity(Intent.createChooser(emailIntent, (pref.getString("language", "0").equals("0"))?getString(R.string.send_mail):getString(R.string.send_mail_esp)));
    }

    public void setNewLayout(int image_icon, String txt, int image){
        Image_icon.add(image_icon);
        text.add(txt);
        Image.add(image);

        Adapter.notifyDataSetChanged();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    public class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.MyViewHolder> {

        ArrayList<Integer> Image_icon = new ArrayList<>();
        ArrayList<String> Text = new ArrayList<>();
        ArrayList<Integer> Image = new ArrayList<>();

        public SettingsAdapter(ArrayList<Integer> Image_icon, ArrayList<String> text, ArrayList<Integer> Image)
        {
            this.Image_icon = Image_icon;
            this.Text = text;
            this.Image = Image;
        }

        @Override
        public SettingsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View card_view = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewlayout, parent, false);
            return new SettingsAdapter.MyViewHolder(card_view);
        }

        @Override
        public void onBindViewHolder(SettingsAdapter.MyViewHolder holder, final int position)
        {
            try {
                holder.text.setText(Text.get(position).toString());
                holder.image_icon.setImageResource(Image_icon.get(position));
                holder.image.setImageResource(Image.get(position));

                holder.image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        action(position);
                    }
                });
            } catch(Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return Image_icon.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView text;
            ImageView image_icon, image;

            public MyViewHolder(View view) {
                super(view);
                image_icon = (ImageView) view.findViewById(R.id.image_icon);
                text = (TextView) view.findViewById(R.id.text);
                image = (ImageView) view.findViewById(R.id.next);
            }
        }
    }

    public void inflateLanguageSelectLayout(){
        LayoutInflater inflater = getLayoutInflater();
        languageSelectView = inflater.inflate(R.layout.language_select_layout, null);
        radioGroup = (RadioGroup) languageSelectView.findViewById(R.id.radioGroup);
        final TextView languageSelectTitle = languageSelectView.findViewById(R.id.select_language_layout_title);
        final RadioButton english = (RadioButton) languageSelectView.findViewById(R.id.english);
        final RadioButton spanish = (RadioButton) languageSelectView.findViewById(R.id.spanish);
        Button submit = (Button) languageSelectView.findViewById(R.id.submit);
        submit.setText((pref.getString("language", "0").equals("0"))?R.string.save:R.string.save_esp);
        final ImageView close = (ImageView) languageSelectView.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                languageSelectDialog.dismiss();
            }
        });

        languageSelectTitle.setText((pref.getString("language", "0").equals("0"))?R.string.change_language:R.string.change_language_esp);

        selectLanguageAlert = new AlertDialog.Builder(activity);
        // this is set the view from XML inside AlertDialog
        selectLanguageAlert.setView(languageSelectView);
        // disallow cancel of AlertDialog on click of back button and outside touch
        selectLanguageAlert.setCancelable(false);
        languageSelectDialog = selectLanguageAlert.create();

        languageSelectDialog.show();
        if(pref.getString("language", "0").equals("1")){
            radioGroup.check(R.id.spanish);
        } else {
            radioGroup.check(R.id.english);
        }

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                languageSelectDialog.dismiss();
                int checkedId = radioGroup.getCheckedRadioButtonId();
                int checked = Integer.parseInt(pref.getString("language", "0"));
                int checked_btn = R.id.english;
                if(checked == 0){
                    checked_btn = R.id.english;
                } else {
                    checked_btn = R.id.spanish;
                }

                if(checked_btn != checkedId){
                    switch (checkedId){
                        case R.id.english:
                            editor.putString("language", "0");
                            editor.apply();
                            break;

                        case R.id.spanish:
                            editor.putString("language", "1");
                            editor.apply();
                            break;
                    }

                    Intent intent = getActivity().getIntent();
                    getActivity().finish();
                    startActivity(intent);
                }
            }
        });
    }

    public void action(int position){
        switch (position){
            case 0:
                inflateLanguageSelectLayout();
                break;

            case 1:
                sendMail();
                break;

            default:
                break;
        }
    }

    public void changeLocale(String locale){
        Resources res = getActivity().getResources();
        // Change locale settings in the app.
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.setLocale(new Locale(locale.toLowerCase())); // API 17+ only.
        // Use conf.locale = new Locale(...) if targeting lower versions
        res.updateConfiguration(conf, dm);
    }
}