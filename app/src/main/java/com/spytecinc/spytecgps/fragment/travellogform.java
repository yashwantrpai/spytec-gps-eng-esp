package com.spytecinc.spytecgps.fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.spytecinc.spytecgps.activity.HomeActivity;
import com.spytecinc.spytecgps.db.DatabaseHelper;
import com.spytecinc.spytecgps.db.DatabaseHelper;
import com.spytecinc.spytecgps.R;
import com.spytecinc.spytecgps.activity.HomeActivity;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class travellogform extends Fragment {

    public travellogform() {
        // Required empty public constructor
    }

    // Travel Log Date-range activity
    private static final String TAG = "Travellog1-dateRange";
    // public static ArrayList travellog_values = new ArrayList();
    Context context;
    DatePickerDialog datePickerDialog;
    DatabaseHelper dbHelper;
    public SharedPreferences pref;
    String dataPoint_str;
    String driver_str;
    public SharedPreferences.Editor editor;
    public static Button submit;

    public static TextView travellog_title, start_date_title, end_date_title, datapoint_title, driver_title;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.travellog1, container, false);
        context = this.getContext();
        dbHelper = new DatabaseHelper();

        pref = context.getSharedPreferences("DataStore", MODE_PRIVATE);
        editor = pref.edit();

        travellog_title = (TextView) rootView.findViewById(R.id.travellog_title);
        travellog_title.setText((pref.getString("language", "0").equals("0"))?R.string.travellog:R.string.travellog_esp);
        start_date_title = (TextView) rootView.findViewById(R.id.start_date_title);
        start_date_title.setText((pref.getString("language", "0").equals("0"))?R.string.start_date:R.string.start_date_esp);
        end_date_title = (TextView) rootView.findViewById(R.id.end_date_title);
        end_date_title.setText((pref.getString("language", "0").equals("0"))?R.string.end_date:R.string.end_date_esp);
        datapoint_title = (TextView) rootView.findViewById(R.id.datapoint_title);
        datapoint_title.setText((pref.getString("language", "0").equals("0"))?R.string.datapoint:R.string.datapoint_esp);
        driver_title = (TextView) rootView.findViewById(R.id.driver_title);
        driver_title.setText((pref.getString("language", "0").equals("0"))?R.string.driver:R.string.driver_esp);

        submit = (Button) rootView.findViewById(R.id.submit);
        submit.setText((pref.getString("language", "0").equals("0"))?R.string.submit:R.string.submit_esp);

        if(pref.getString("curfile", "") == null || !HomeActivity.equalStrings(pref.getString("curfile", "").toString(), "travellogform")) {
            editor.putString("startdate", "");
            editor.putString("enddate", "");
            editor.putString("datapoint", "");
            editor.putString("driver", "");
            editor.putString("curfile", "travellogform");
        }
        editor.commit();


        // Setting default values
        // Prefilling start date and end date

        final TextView SD = (TextView) rootView.findViewById(R.id.startDate_text);
        final TextView ED = (TextView) rootView.findViewById(R.id.endDate_text);
        final Spinner DataPoint = (Spinner) rootView.findViewById(R.id.selectDatapoint_text);
        DataPoint.setSelection(0);
        final Spinner Driver = (Spinner) rootView.findViewById(R.id.selectDriver_text);
        Driver.setSelection(0);

        SD.setOnClickListener(new View.OnClickListener() {
            Calendar c = Calendar.getInstance();
            int day = c.get(Calendar.DAY_OF_MONTH), month = c.get(Calendar.MONTH), year = c.get(Calendar.YEAR);
            String dt;
            @Override
            public void onClick(View view) {
                if(pref.getString("startdate", "") != null && pref.getString("startdate", "") != ""){
                    String[] date_split = pref.getString("startdate", "").split("/");
                    day = Integer.parseInt(date_split[1]);
                    month = Integer.parseInt(date_split[0]);
                    year = Integer.parseInt(date_split[2]);
                }

                // Click action
                datePickerDialog = new DatePickerDialog(context, R.style.datepicker, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int selectedYear, int selectedMonth, int selectedDay) {
                        Date date = new Date();
                        Calendar cal = Calendar.getInstance();
                        cal.set(selectedYear,selectedMonth,selectedDay);
                        date.setTime(cal.getTime().getTime());

                        SimpleDateFormat dateFormat = new SimpleDateFormat((pref.getString("language", "0").equals("0"))?getString(R.string.date_format):getString(R.string.date_format_esp));
                        dt = dateFormat.format(date);
                        SD.setText(dt);
                        // travellog_values.set(0,SD.getText());
                        editor.putString("startdate", SD.getText().toString());
                        editor.commit();
                        Log.d("Selected Date", dt);
                    }
                }, year, month - 1, day);
                // Set Max date and Min date for add date
                datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                // datePickerDialog.setTitle("Pick start date");
                datePickerDialog.show();
            }
        });

        ED.setOnClickListener(new View.OnClickListener() {
            Calendar c = Calendar.getInstance();
            int day = c.get(Calendar.DAY_OF_MONTH), month = c.get(Calendar.MONTH), year = c.get(Calendar.YEAR);
            String dt;
            @Override
            public void onClick(View view) {
                if(pref.getString("enddate", "") != null && pref.getString("enddate", "") != ""){
                    String[] date_split = pref.getString("enddate", "").split("/");
                    day = Integer.parseInt(date_split[1]);
                    month = Integer.parseInt(date_split[0]);
                    year = Integer.parseInt(date_split[2]);
                }

                // Click action
                datePickerDialog = new DatePickerDialog(context, R.style.datepicker, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int selectedYear, int selectedMonth, int selectedDay) {
                        Date date = new Date();
                        Calendar cal = Calendar.getInstance();
                        cal.set(selectedYear,selectedMonth,selectedDay);
                        date.setTime(cal.getTime().getTime());

                        SimpleDateFormat dateFormat = new SimpleDateFormat((pref.getString("language", "0").equals("0"))?getString(R.string.date_format):getString(R.string.date_format_esp));
                        dt = dateFormat.format(date);
                        ED.setText(dt);
                        // travellog_values.set(1,ED.getText());
                        editor.putString("enddate", ED.getText().toString());
                        editor.commit();
                        Log.d("Selected Date", dt);
                    }
                }, year, month - 1, day);
                // Set Max date and Min date for add date
                datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                // datePickerDialog.setTitle("Pick end date");
                datePickerDialog.show();
            }
        });

        // End Date
        Date date = new Date();
        SimpleDateFormat curFormater = new SimpleDateFormat((pref.getString("language", "0").equals("0"))?getString(R.string.date_format):getString(R.string.date_format_esp));
        String EndDate = curFormater.format(date);
        Log.d(TAG, EndDate);
        ED.setText(EndDate);
        SD.setText(EndDate);

        // driver adapter and selected item
        ArrayAdapter<CharSequence> dataPoint_adapter = new ArrayAdapter<CharSequence>(context, R.layout.spinner_item, (pref.getString("language", "0").equals("0"))?getResources().getStringArray(R.array.datapoints):getResources().getStringArray(R.array.datapoints_esp));
        dataPoint_adapter.setDropDownViewResource(R.layout.spinner_item_dropdown);
        DataPoint.setAdapter(dataPoint_adapter);
        DataPoint.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                dataPoint_str = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        String drivers_str = pref.getString("drivers", "");
        String drivers[] = null;
        try {
            if(drivers_str != null && drivers_str!= ""){
                JSONArray drivers_arr = new JSONArray(drivers_str);
                if(drivers_arr != null){
                    drivers = new String[drivers_arr.length()];
                    for(int i = 0; i < drivers_arr.length(); i++){
                        drivers[i] = drivers_arr.get(i).toString();
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(drivers != null && drivers.length > 0) {
            ArrayAdapter<CharSequence> driver_adapter = new ArrayAdapter<CharSequence>(context, R.layout.spinner_item, drivers);
            driver_adapter.setDropDownViewResource(R.layout.spinner_item_dropdown);
            Driver.setAdapter(driver_adapter);
            Driver.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    driver_str = adapterView.getItemAtPosition(i).toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        } else {
            Log.w(TAG, "home.Name is null");
            final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            final AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialogBuilder.setTitle((pref.getString("language", "0").equals("0"))?R.string.data_sync_issue:R.string.data_sync_issue_esp);
            alertDialogBuilder.setMessage((pref.getString("language", "0").equals("0"))?R.string.re_login_and_try_again:R.string.re_login_and_try_again_esp);
            alertDialogBuilder.setNeutralButton(getActivity().getResources().getString((pref.getString("language", "0").equals("0"))?R.string.ok:R.string.ok_esp),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialogBuilder.show();
        }

        editor.putString("startdate", EndDate);
        editor.putString("enddate", EndDate);
        editor.commit();

        // Back button
        ImageView back_btn = (ImageView) rootView.findViewById(R.id.back);
        back_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Fragment reportsFragment = new reports();
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                editor.putString("CURRENT_TAG", HomeActivity.TAG_REPORTS);
                editor.commit();
                fragmentTransaction.replace(R.id.frame, reportsFragment, HomeActivity.TAG_REPORTS);
                fragmentTransaction.commitAllowingStateLoss();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if((SD.getText().toString().matches("")) || (ED.getText().toString().matches(""))){
                    ArrayList no_values = new ArrayList();
                    no_values.add("");
                    no_values.add("");
                    if(SD.getText().toString().matches("")){
                        no_values.set(0,"Start Date");
                    }
                    if(ED.getText().toString().matches("")){
                        no_values.set(1,"End Date");
                    }
                    AlertDialog Alert = new AlertDialog.Builder(context).create();
                    Alert.setMessage(Html.fromHtml((pref.getString("language", "0").equals("0"))?getContext().getResources().getString(R.string.please_fill_values):getContext().getResources().getString(R.string.please_fill_values_esp)+": <br>"+no_values.get(0).toString()+"<br>"+no_values.get(1).toString()));
                    Alert.setButton(AlertDialog.BUTTON_NEUTRAL, getActivity().getResources().getString((pref.getString("language", "0").equals("0"))?R.string.ok:R.string.ok_esp),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    Alert.show();
                }
                else{
                    Date sD = new Date(SD.getText().toString());
                    Date eD = new Date(ED.getText().toString());
                    if (eD.getTime() >= sD.getTime()) {
                        editor.putString("datapoint", dataPoint_str);
                        editor.putString("driver", driver_str);
                        editor.commit();
                        Fragment travellogFragment = new travellog();
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                        editor.putString("CURRENT_TAG", HomeActivity.TAG_TRAVELLOG);
                        editor.commit();
                        fragmentTransaction.replace(R.id.frame, travellogFragment, HomeActivity.TAG_TRAVELLOG);
                        fragmentTransaction.commitAllowingStateLoss();
                    } else {
                        AlertDialog DateAlert = new AlertDialog.Builder(getActivity()).create();
                        DateAlert.setMessage(getActivity().getResources().getString((pref.getString("language", "0").equals("0"))?R.string.end_date_greater_than_start_date:R.string.end_date_greater_than_start_date_esp));
                        DateAlert.setButton(AlertDialog.BUTTON_NEUTRAL, getActivity().getResources().getString((pref.getString("language", "0").equals("0"))?R.string.ok:R.string.ok_esp),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        DateAlert.show();
                    }
                }
            }
        });

        return rootView;
    }
}
